<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class versiontable extends Model
{
    //table name
    protected $table ='versiontable';
    //primary  key
    public $primarykey ='id';
    //timestamps
    public $timestamps=true;

    protected $fillable=['version','launchdate','os','status'];
}
