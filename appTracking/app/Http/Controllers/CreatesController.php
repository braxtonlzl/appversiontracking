<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\versiontable;
class CreatesController extends Controller
{

    function postdata(Request $request){

      $validation =Validator::make($request->all(),[
        'version' => 'required',
        'launchdate'=> 'nullable|date',
        'os'=>'required|not_in:0',
        'status'=>'required|not_in:0'
      ]);
      $error_array = array();
      $success_output = '';
      if ($validation->fails())
      {
          foreach($validation->messages()->getMessages() as $field_name => $messages)
          {
              $error_array[] = $messages;
          }
      }
      else
      {
          if($request->get('button_action') == "insert")
          {
              $versiontable = new versiontable([
                'version'=>$request->get('version'),
                'launchdate'=>$request->get('launchdate'),
                'os'=>$request->get('os'),
                'status'=>$request->get('status')
              ]);
              $versiontable->save();
              $success_output = '<div class="alert alert-success">Data Inserted</div>';
          }

          if($request->get('button_action') == 'update')
          {
            $versiontable = versiontable::find($request->get('version_id'));
            $versiontable->version = $request->get('version');
            $versiontable->launchdate = $request->get('launchdate');
            $versiontable->os = $request->get('os');
            $versiontable->status = $request->get('status');
            $versiontable->save();
            $success_output = '<div class="alert alert-success">Data Updated</div>';
          }
      }
      $output = array(
          'error'     =>  $error_array,
          'success'   =>  $success_output
      );
      echo json_encode($output);
    }
      function removedata(Request $request)
      {
        $versiontable = versiontable::find($request->input('id'));
        if($versiontable->delete())
        {
          echo 'Data Deleted';
        }
      }
      public function fetchdata(Request $request)
      {
        $id = $request->input('id');
          $versiontable = versiontable::find($id);
          $output = array(
              'version'    =>  isset($versiontable->version) ? $versiontable->version : null,
              'launchdate'     =>  isset($versiontable->launchdate) ? $versiontable->launchdate : null,
              'os'     =>  isset($versiontable->os) ? $versiontable->os : null,
              'status'     =>  isset($versiontable->status) ? $versiontable->status : null
          );
          echo json_encode($output);
      }
  }
