<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Datatables;
use App\versiontable;
class VersionsController extends Controller
{
    protected $namespace = 'App\Student\Controllers';

    Public function index(){
      return view('pages.version',compact('versiontable'));
    }

    public function gettable()
    {
      $versiontables = versiontable::select('id','version','os','launchdate','created_at','updated_at','status');
      return Datatables::of($versiontables)
        ->addColumn('action',function($versiontable){
          return '<a href="#" class="btn btn-xs btn-primary edit" id="'.$versiontable->id.'"><i class="glyphicon glyphicon-edit"></i> Edit</a>
          <a href="#" class="btn btn-xs btn-danger delete" id="'.$versiontable->id.'"><i class="glyphicon glyphicon-remove"></i> Delete</a>';
        })
      ->make(true);
    }
  }
