<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/','PagesController@index');

Route::get('version','VersionsController@index')->name('pages.version');

Route::get('index', 'VersionsController@index');

Route::get('gettable', 'VersionsController@gettable')->name('get.table');

Route::post('create',['uses'=>'CreatesController@store']);

Route::post('add','CreatesController@postdata')->name('ajaxdata.postdata');

Route::get('add','CreatesController@fetchdata')->name('ajaxdata.fetchdata');

Route::get('delete', 'CreatesController@removedata')->name('ajaxdata.removedata');
