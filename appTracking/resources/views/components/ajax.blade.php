<script>
$(document).ready(function() {
      $('#mytable').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!!route('get.table')!!}',
      columns: [
               { data: 'id', name: 'id' },
               { data: 'version', name: 'version' },
               { data: 'os', name: 'os' },
               { data: 'launchdate', name: 'launchdate' },
               { data: 'created_at', name: 'created_at' },
               { data: 'updated_at' , name: 'updated_at'},
               { data: 'status', name: 'status' },
               { data: 'action', orderable: false, searchable: false}
            ]
   });
   $('#add_data').click(function(){
     $('#versionModal').modal('show');
     $('#version_form')[0].reset();
     $('#form_output').html('');
     $('#button_action').val('insert');
     $('#action').val('Add');
   });
   $('#version_form').on('submit',function(event){
     event.preventDefault();
     var form_data = $(this).serialize();
     
     $.ajax({
       url:'{{ route("ajaxdata.postdata") }}',
       method:"POST",
       data:form_data,
       dataType:"json",
       success:function(data)
       {
         if(data.error.length>0){
           var error_html = '';
           for(var count = 0; count < data.error.length; count++)
           {
               error_html += '<div class="alert alert-danger">'+data.error[count]+'</div>';
           }
            $('#form_output').html(error_html);
       }
       else
       {
           $('#form_output').html(data.success);
           $('#version_form')[0].reset();
           $('#action').val('Add');
           $('.modal-title').text('Add Data');
           $('#button_action').val('insert');
           $('#versiontable_table').DataTable().ajax.reload();
         }
       }
     })
   });
   $(document).on('click', '.edit', function(){
    var id = $(this).attr("id");
    $('#form_output').html('');
    $.ajax({
        url:"{{route('ajaxdata.fetchdata')}}",
        method:'get',
        data:{id:id},
        dataType:'json',
        success:function(data)
        {
            $('#version').val(data.version);
            $('#launchdate').val(data.launchdate);
            $('#os').val(data.os);
            $('#status').val(data.status);
            $('#version_id').val(id);
            $('#versionModal').modal('show');
            $('#action').val('Edit');
            $('.modal-title').text('Edit Data');
            $('#button_action').val('update');
        }
    })
});
  $(document).on('click', '.delete', function(){
    var id = $(this).attr('id');
    if(confirm("Are you sure you want to Delete this data?"))
    {
      $.ajax({
          url:"{{route('ajaxdata.removedata')}}",
          mehtod:"get",
          data:{id:id},
          success:function(data)
          {
              alert(data);
              $('#versiontable_table').DataTable().ajax.reload();
            }
          })
        }
        else
        {
          return false;
        }
      });
});
</script>
