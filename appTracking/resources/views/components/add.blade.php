<div align="right">
      <button type="button" name="add" id="add_data" class="btn btn-success btn-sm">Add</button>
</div>


<div id="versionModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" id="version_form">
                <div class="modal-header text-center">
                  <h4 class="modal-title text-cente ">Add Data</h4>
                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    {{csrf_field()}}
                    <span id="form_output"></span>
                    <div class="form-group">
                        <label>Version</label>
                        <input type="decimal" name="version" id="version" class="form-control.step=>'0.01'" />
                    </div>
                    <div class="form-group">
                        <label>Launch Date</label>
                        <input type="date" name="launchdate" id="launchdate" class="form-control" />
                    </div>

                    <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <label class="input-group-text" for="inputGroupSelect01">OS</label>
                    </div>
                    <select name='os' class="custom-select" id="inputGroupSelect01">
                      <option selected value="0">Choose...</option>
                      <option value="iOS">iOS</option>
                      <option value="Android">Android</option>
                    </select>
                    </div>
                    <div class="input-group mb-3">

                    <div class="input-group-prepend">
                      <label class="input-group-text" for="inputGroupSelect02">Status</label>
                    </div>
                    <select name='status' class="custom-select" id="inputGroupSelect02">
                      <option selected value="0">Choose...</option>
                      <option value="Live">Live</option>
                      <option value="Pending">Pending</option>
                      <option value="Deactive">Deactive</option>
                    </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="version_id" id="version_id" value="" />
                    <input type="hidden" name="button_action" id="button_action" value="insert">
                    <input type="submit" name="submit" id="action" value="Add" class="btn btn-info">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<br>
