@extends ('layouts.app')

@section('title','View')

@section('content')
  <div class="container">
    @include('components.add')
    @include('components.table')
  </div>

@endsection
