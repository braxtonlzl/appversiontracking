<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>{{config('app.name'.'AppVersionTracking')}}</title>
      @include('components.css')
  </head>
  <body>
    @include('components.navbar')
    @yield('content')
    @include('components.js')
    @include('components.ajax')
  </body>
</html>
