<?php

use Faker\Generator as Faker;

$factory->define(App\Versiontable::class, function (Faker $faker) {
    return [
        'version'=>$faker->randomDigit(),
        'launchdate'=>$faker->date($format = 'Y-m-d', $max = 'now'),
        'os'=>$faker->creditCardType(),
        'status'=>$faker->creditCardType(),
    ];
});
