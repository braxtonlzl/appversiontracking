<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Versiontable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('versiontable', function (Blueprint $versiontable) {
            $versiontable->increments('id');
            $versiontable->decimal('version');
            $versiontable->date('launchdate');
            $versiontable->Timestamp('created_at')->nullable();
            $versiontable->Timestamp('updated_at')->nullable();
            $versiontable->string('os');
            $versiontable->string('status');
          });
        }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
